#' @title `rtsviz.calpat` - Sample plugin for the `rtsviz` package.
#' 
#' @description This user friendly shiny application allows to interactively 
#'	explore calendar patterns. Investigate the historical impact on returns and volatility
#'	for given look-back and look-ahead periods. User can either specify a calendar pattern or
#'	import the historical dates for analysis.
#'
#' 
#' @examples
#' \dontrun{ 
#'	library(quantmod)
#'		
#'	# enable persistent time series data storage with `rtsdata` package
#'	library(rtsdata)
#'	
#'	# load `rtsviz.calpat` package
#'	library(rtsviz.calpat)
#'		
#'	# start Shiny interface
#'	launchApp()
#'		
#'		
#'	# ---------------
#'	# start Calendar Patterns app in the `rtsviz` interface
#'	# please note this package registers 'calpat' app as `rtsviz` plugin
#'	# ---------------	
#'	# load `rtsviz` package; will **overwrite** `launchApp` function
#'	library(rtsviz)
#'		
#'	# start Shiny interface
#'	launchApp(highlight.item='calpat')	
#' }
#' 
#' 
#' 
#' @import shiny
#' @import TTR
#'
#' @name rtsviz.calpat
#' @docType package
#' 
NULL



rtsviz.calpat.options = function() getOption('rtsviz.calpat.options')

###############################################################################
#' Load FOMC meeting dates
#'
#' @return list of dates
#' @export 
###############################################################################
get.fomc.dates = function() {
	if(is.null(rtsviz.calpat.options()$fomc.dates)) {
		load(system.file('data', 'fomc.dates.Rdata', package='rtsviz.calpat'))
		set.options('rtsviz.calpat.options', fomc.dates=as.Date(fomc.dates))
	}
	rtsviz.calpat.options()$fomc.dates
}


###############################################################################
#' Load FRED series ids
#'
#' @return list of FRED series ids
#' @export 
###############################################################################
get.fred.ids = function() {
	if(is.null(rtsviz.calpat.options()$fred.ids)) {
		load(system.file('data', 'fred.ids.Rdata', package='rtsviz.calpat'))
		set.options('rtsviz.calpat.options', fred.ids=fred.ids)
	}
	rtsviz.calpat.options()$fred.ids
}		

