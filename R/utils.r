###############################################################################
# Common functions used internally
###############################################################################
len = function(x) length(x)

mlast = function(m, n=1) 
	if( is.matrix(m) ) {
		m[(nrow(m)-n+1):nrow(m), ,drop=FALSE] 
	} else { 
		m[(len(m)-n+1):len(m)]
	}

mlag = function(m, nlag=1) 
  if( is.matrix(m) ) {
    n = nrow(m)
    if(nlag > 0) {
      m[(nlag+1):n,] = m[1:(n-nlag),]
      m[1:nlag,] = NA
    } else if(nlag < 0) {
      m[1:(n+nlag),] = m[(1-nlag):n,]
      m[(n+nlag+1):n,] = NA
    } 
	m
  } else { # vector
    n = len(m)
    if(nlag > 0) {
      m[(nlag+1):n] = m[1:(n-nlag)]
      m[1:nlag] = NA
    } else if(nlag < 0) {
      m[1:(n+nlag)] = m[(1-nlag):n]
      m[(n+nlag+1):n] = NA
    }
	m	
  }

spl = function(s, delim = ',') unlist(strsplit(s,delim))

join = function(s, delim = '') paste(s, collapse = delim)

rep.row = function(m, nr) matrix(m, nrow=nr, ncol=len(m), byrow=TRUE)

trim = function(s) sub('\\s+$', '', sub('^\\s+', '', s))

make.copy = function(x, default) { out = x; out[] = default; out }

	  
iif = function(cond, truepart, falsepart) 
	if(len(cond) == 1) { 
		if(cond) truepart else falsepart 
	} else {  
		if(length(falsepart) == 1) falsepart = make.copy(cond, falsepart)
    	
		cond[is.na(cond) | is.nan(cond) | is.infinite(cond)] = FALSE
			
		if(length(truepart) == 1) 
			falsepart[cond] = truepart 
		else
			falsepart[cond] = zoo::coredata(truepart)[cond]
		falsepart
	}
    
ifnull = function(x, y) iif(is.null(x), y, x)

ifna = function(x, y) iif(is.na(x) | is.nan(x) | is.infinite(x), y, x)

ifna.prev = function(x) zoo::na.locf(zoo::coredata(x),fromLast=FALSE,na.rm=FALSE)

# backfill from left to right
ifna.prevx = function(x) zoo::na.locf(zoo::coredata(x),fromLast=TRUE,na.rm=FALSE)

to.date = function(x) if(class(x)[1] != 'Date') as.Date(x, format='%Y-%m-%d') else x

date.month = function(dates) as.POSIXlt(dates)$mon + 1

date.year = function (dates) as.POSIXlt(dates)$year + 1900

compute.stats = function(data, fns) {
	out = matrix(double(), len(fns), len(data))
		colnames(out) = names(data)
		rownames(out) = names(fns)
	for(c in 1:len(data))
		for(r in 1:len(fns))
			out[r,c] = match.fun(fns[[r]])( data[[c]] )
	out
}

count = function(x, side=2) iif(is.matrix(x), apply(!is.na(x), side, sum), sum( !is.na(x) )) 

chr <- function(n) { rawToChar(as.raw(n)) }

make.random.string <- function(nbits = 256) { chr( stats::runif(nbits/8, 1, 255) ) }


# shortcut for xts index
indexts = function(x) {
	temp = xts::.index(x)
	class(temp) = c('POSIXct', 'POSIXt')
  
	type = xts::indexClass(x)[1]
	if( type == 'Date' || type == 'yearmon' || type == 'yearqtr')
		temp = as.Date(temp)
	temp
}



# set global options
set.options = function(key, ..., overwrite=TRUE) {
	values = list(...)
	if( len(values) == 1 && is.null(names(values))) values = values[[1]]
	temp = ifnull(options()[[key]], list())
	
	for(i in names(values))
		if(overwrite)
			temp[[i]] = values[[i]]
		else {
			if( is.null(temp[[i]]) )
				temp[[i]] = values[[i]]
		}
	
	options(make.list(key, temp))
}

# make list
make.list = function(key, value) {
	out = list()
	out[[key]] = value
	out
}

write.file = function(..., file) cat(..., file=file)

read.file = function(file) readChar(file, file.info(file)$size)
 


# capture cat function output to string
table2str = function(x,row.names=FALSE,col.names=TRUE,...) {
	file = open.string.buffer()

	suppressWarnings(
		utils::write.table(x,file,sep='\t',row.names=row.names,col.names=col.names,...)
	)
		
	out = string.buffer(file)
	
	close(file)
	file = NULL
	
	out
}

open.string.buffer = function() rawConnection(raw(0L), open='w')
string.buffer = function(sb) rawToChar(rawConnectionValue(sb))

###############################################################################
# Formating
###############################################################################
to.percent = function(x, digits=1) iif(is.na(x), '', paste0(round(100*x, digits),'%'))	  
to.nice = function(x) iif(is.na(x), '', round(x[1],2))
to.nice.date = function(x) iif(is.na(x), '', paste(x))


###############################################################################
# Shiny
###############################################################################

knitr.add.label = function(df, label) gsub('">( )*</th',paste0('">', label, '</th'), knitr::kable(df, "html", escape = FALSE))

add.toc = function(label, id, labels) tagList(
	tags$h3(label)
	,tags$ul(class="list-unstyled"
	,lapply(1:len(labels), function(i) tags$li(tags$a(href=paste0('#',id,i),labels[i])))
	))
	
add.toc.link = function(id, label, add.break=TRUE) tagList(
	if(add.break) tags$br() else NULL				
	,tags$span(class='anchor', id=id)
	,tags$h3(label, tags$a(href="#top", icon('arrow-circle-up')))
)

add.pdf.table = function(df) {
	grid::grid.newpage()
	g2 =  gridExtra::tableGrob(df)		
	g2$widths = grid::unit(rep(1/ncol(g2), ncol(g2)), "npc")
	grid::grid.draw(g2)
}	
	
add.csv.table = function(label, df, file, append = TRUE) {
	cat(label, '\n', file = file, append=append)
			
	# Warning in write.csv attempt to set 'append' ignored
	suppressWarnings(
		utils::write.table(df, sep=',', file=file, row.names = TRUE, col.names = NA, quote = FALSE, append = TRUE)
	)
	
	cat('\n', file = file, append = TRUE)
}	
	
add.xts = function(df, file) {
	cat("Date", file = file, append = FALSE)
    write.table(df, sep = ",", row.names = format(indexts(df)),
        col.names = NA, file = file, append = T, quote = F)
}


###############################################################################
# Asset Performance / Risk functions
###############################################################################

discrete.return = function(data) { ret = data / mlag(data) - 1; as.vector(ret[-1]) }

days.range = function(data) {
	period = diff(range(indexts(data)))
		units(period) = "days"
	as.double(period)
}

annualized.return = function(data, ret = discrete.return(data)) prod(1+ifna(ret,0))^(365 / days.range(data)) - 1

annualized.risk = function(data, ret = discrete.return(data)) {
	freq = stats::median(diff(indexts(data)))
		units(freq) = "days"
		freq = as.double(freq)
		
	adj = 252
	if(freq > 1) {
		adj = round(365 / freq)
	} else {
		adj = 252/freq
	}
			
	stats::sd(ret,na.rm=T) * sqrt(adj) # not best approximation
}

maximum.drawdown = function(data) {
	x = as.vector(data)
	n = 1:len(x)
	dd = x / cummax(x) - 1
	
	maxdd = min(dd)
		dmin = n[dd==maxdd][1]
		dstart = max(n[1:dmin][dd[1:dmin] == 0])+1
		dend = min(len(x), n[dmin:len(x)][dd[dmin:len(x)] == 0])
		
		Trough = From = To = NA
		if(xts::is.xts(data)) {
			Trough = indexts(data)[dmin]
			From = indexts(data)[dstart]
			To = indexts(data)[dend]
		}
		
	list(Depth=maxdd, From=From, To=To)
}






