.onLoad <- function(libname, pkgname) {
	# register rtsviz plugins, do not overwrite if already registered
	rtsviz::register.rtsviz.plugin('calpat',
		'Calendar Pattern','Calendar Pattern Tool',
		'Examine performance and volatility for selected calendar patterns',
		'Examples', calpat.ui, calpat.server
		,overwrite=FALSE)
	
	set.options('rtsviz.calpat.options', list())

	# add resources folder to shiny's search path
	# make sure www path is different from other package
	shiny::addResourcePath('www.calpat', system.file('www', package = 'rtsviz.calpat'))

	
	invisible()
}



