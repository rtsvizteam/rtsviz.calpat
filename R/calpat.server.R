###############################################################################
#' Calendar Pattern shiny app Server object
#'
#' Create the 'calpat' shiny Server
#'
#' @param input list of input objects for shiny app
#' @param output list of output objects for shiny app
#' @param session session for shiny app
#'
#' @return shiny Server object
#'
#' @export
###############################################################################
calpat.server = function(input, output, session) {
	#--------------------------
	# define variables
	#--------------------------
	session = session
	app.id = rtsviz::mod_appID("calpat.applicationID")
	getLogs = rtsviz::getLogs(session)
	
    #*****************************************************************
    # Data
    #******************************************************************    			
	data_mod = callModule(rtsviz::mod_load.data, "inputSymbol", app.id, reactive(input$applicationID), getLogs, p.start=0, p.end=0.6)
	valid = data_mod$valid
	getStocks = reactive({ data_mod$getStocks()[1] })
	
	getData = data_mod$getData

	
	# get data
  	getData1 <- reactive({
		# control if any action needs to be done
		if(!valid()) return(NULL)
		if(is.null( (data = getData()) )) return(NULL)
		
		temp = rtsviz::make.stock(data[[ getStocks() ]])[data_mod$getDates()]
		#temp = make.stock(data[[ getStocks()[1] ]])[data_mod$getDates()]

		if(days.range(temp) < 365 * 1.8) stop('At least 2 years are required to run the Seasonality Analysis.')
		
		temp
	})

	
    #*****************************************************************
    # Dates
    #******************************************************************    	
  	getDates <- reactive({
		# control if any action needs to be done
		if(is.null( (data = getData1()) )) return(NULL)
		
		logs = getLogs('Get Dates:',0.6,0.7)
		dates = zoo::index(data)
		ticker = getStocks()
		
		if(input$dateSource == 'custom') {			
			tryCatch({points = dates[rtsdate::custom.date(input$dateExpr, dates)]
			}, error = function(e) { 
				logs$log.err('Error: Problem running following custom expression', input$dateExpr, percent=0/100)		
				stop(paste('Problem running following custom expression', input$dateExpr,as.character(e))) 
			})					
		} else if(input$dateSource == 'FOMC') {
			points = get.fomc.dates()
		} else if(input$dateSource == 'FRED') {
			tryCatch({points = get.FRED.release.dates(input$fredID,format(dates[1]-360, '%Y-%m-%d'))
			}, error = function(e) { 
				logs$log.err('Error: Problem loading dates from FRED, id ', input$fredID, percent=0/100)		
				stop(paste('Problem loading dates from FRED, id ', input$fredID,as.character(e))) 
			})					
		} else if(input$dateSource == 'user data') {
			infile = input$userDatefile
			infile = isolate({input$userDatefile_temp})		
			if (is.null(infile)) return(NULL)
		
			tryCatch({temp = utils::read.csv(infile$datapath, stringsAsFactors=FALSE,row.names=NULL)
			}, error = function(e) { 
				logs$log.err('Error: Problem loading user file', infile, percent=0/100)		
				stop(paste('Problem loading user file',infile,as.character(e))) 
			})
			temp = anytime::anydate(temp[[1]])
			points = date[!is.na(temp)]
		} else if(input$dateSource == 'earnings') {
			tryCatch({points = get.div.eps.info()$eps.dates
			}, error = function(e) { 
				logs$log.err('Error: Problem loading Earnings dates for ', ticker, percent=0/100)		
				stop(paste('Problem loading Earnings dates for ', ticker, as.character(e))) 
			})								
		} else if(input$dateSource == 'dividends') {
			tryCatch({points = get.div.eps.info()$div.dates
			}, error = function(e) { 
				logs$log.err('Error: Problem loading Dividends dates for ', ticker, percent=0/100)		
				stop(paste('Problem loading Dividends dates for ', ticker, as.character(e))) 
			})								
		}
		
		# map points to dates
		if(is.null(points)) index = NULL
		else
			tryCatch({index = map.dates.points(dates, points, max.diff=5)
			}, error = function(e) { 
				logs$log.err('Error: Problem maping dates', percent=0/100)		
				stop(paste('Problem maping dates', as.character(e))) 
			})

			
		if(is.null(index) | len(index) == 0) {
			logs$log.err('Error: There is no overlap between selected dates and data', percent=0/100)		
			stop(paste('There is no overlap between selected dates and data')) 		
		}
				
		logs$log('End', percent=100/100)
		
		index
	})

	# load EPS and Dividends
	get.div.eps.info <- reactive({
		# control if any action needs to be done
		if(is.null( (data = getData1()) )) return(NULL)
	
		if(input$dateSource == 'earnings' | input$dateSource == 'dividends')		
			div.eps.info(getStocks())
		else
			NULL
	})
	
    #*****************************************************************
    # First run message
    #******************************************************************    	
	callModule(rtsviz::mod_first.help, "firstHelp", reactive(is.null(getData())))	

	
    #*****************************************************************
    # Compute returns
    #*****************************************************************
	getReturns <- reactive({
		# control if any action needs to be done
		if(is.null( (data = getData1()) )) return(NULL)
		
		data.returns(data, getDates(), input$frequency
			,input$nLookBack, input$nLookForward, input$allowOverlap, input$marketFilter)
	})


	
	getReturnsMatrix <- reactive({
		# control if any action needs to be done
		if(is.null( (data = getData1()) )) return(NULL)

		returns.matrix(getReturns(), input$nLookBack, input$nLookForward)
	})
	
		
	
    #*****************************************************************
    # Summary Tab
    #******************************************************************    			
	# Generate a plot	
	output$strategyPlot <- renderPlot({ 
		# control if any action needs to be done
		if(is.null( (data = getData1()) )) return(NULL)				
				
		logs = getLogs('Plot:', p.start=0.7, p.end=1)
		logs$log('Start', percent=0/100)  
				
		calpat.overlayPlot(getStocks(), getReturnsMatrix())
		
		logs$log('End', percent=100/100)		
	})	
	
	# Generate a table
	getTradesTable <- reactive({
		# control if any action needs to be done
		if(is.null( (data = getData1()) )) return(NULL)

		calpat.tradesTable(getStocks(), getReturns(), getReturnsMatrix())
	})
	
	output$summaryHistoricalPerfromance <- reactive({
		# control if any action needs to be done
		if(is.null( (data = getData1()) )) return(NULL)
		
		df1 = getTradesTable()
		kableExtra::kable_styling(
			knitr.add.label(df1, 'Length'),
			c("striped", "hover", "condensed", "responsive"), 
			full_width = FALSE,
			position = 'left'
		)
	})	
	
	output$summaryHistoricalPerfromanceCopy <- renderUI({
		# control if any action needs to be done
		if(is.null( (data = getData1()) )) return(NULL)
		
		df1 = getTradesTable()
		rclipboard::rclipButton("BTNsummaryHistoricalPerfromanceCopy", "Copy", table2str(df1, row.names = TRUE), icon("clipboard"))
	})
	
	

	output$summaryEquityCurve <- renderPlot({ 
		# control if any action needs to be done
		if(is.null( (data = getData1()) )) return(NULL)								
		
		calpat.tradesPlot(getStocks(), getReturnsMatrix())		
	})	
	

    #*****************************************************************
    # Details Tab
    #******************************************************************    	
	getStatsTable <- reactive({
		# control if any action needs to be done
		if(is.null( (data = getData1()) )) return(NULL)

		rtsviz::seasonality.makeStatsTable(getStocks(), getReturns()$ret, getReturnsMatrix())
	})
	
	# Generate a plot
	output$summaryStatsPlot <- renderPlot({ 
		# control if any action needs to be done
		if(is.null( (data = getData1()) )) return(NULL)				

		rtsviz::seasonality.makeStatsPlot(getStocks(), getStatsTable()$raw
			,spl('Sharpe,% Positive,Risk,Avg')
			,paste0('(', input$frequency, ')')
			)
	})	
	
	# Generate a table
	output$summaryStatsTable <- reactive({
		# control if any action needs to be done
		if(is.null( (data = getData1()) )) return(NULL)
		
		#df1 = zoo::coredata(df); rownames(df1) = paste(indexts(df))
		
		df1 = getStatsTable()$format
		kableExtra::kable_styling(
			knitr::kable(df1, "html"),
			c("striped", "hover", "condensed", "responsive"), 
			full_width = FALSE,
			position = 'left'
		)
	})
	
	output$summaryStatsTableCopy <- renderUI({
		# control if any action needs to be done
		if(is.null( (data = getData1()) )) return(NULL)
		
		df1 = getStatsTable()$format		
		rclipboard::rclipButton("BTNsummaryStatsTableCopy", "Copy", table2str(df1, row.names = TRUE), icon("clipboard"))
	})
	
	
	# Generate a table
	getHistoricalTable <- reactive({
		# control if any action needs to be done
		if(is.null( (data = getData1()) )) return(NULL)

		m = getReturnsMatrix()
		m = m[nrow(m):1,,drop=F]
		df1 = apply(m,2,to.percent,2)
			rownames(df1) = rownames(m)
		df1
	})
	
	output$historicalTable <- reactive({
		# control if any action needs to be done
		if(is.null( (data = getData1()) )) return(NULL)

		df1 = getHistoricalTable()
		kableExtra::kable_styling(
			knitr::kable(df1, "html"),
			c("striped", "hover", "condensed", "responsive"), 
			full_width = FALSE,
			position = 'left'
		)
	})

	output$historicalTableCopy <- renderUI({
		# control if any action needs to be done
		if(is.null( (data = getData1()) )) return(NULL)

		df1 = getHistoricalTable()
		rclipboard::rclipButton("BTNhistoricalTableCopy", "Copy", table2str(df1, row.names = TRUE), icon("clipboard"))
	})
	
	#*****************************************************************
    # Stat Tab
    #******************************************************************    	
	callModule(rtsviz::mod_stat.tab, "statPanel"
		,getStocks
		,getData1
		,getReturnsMatrix
	)
	
   #*****************************************************************
    # History Tab
    #******************************************************************    	
	# Insert the right number of plot output objects into the web page	
	output$detailsDatesCharts <- renderUI({ 
		# control if any action needs to be done
		if(is.null( (data = getData1()) )) return(NULL)
		
		rets = getReturns()
			# only show 20 most recent episodes
			index = rets$index1
			data = rets$data
		
		# [Buttons](https://getbootstrap.com/docs/4.0/components/buttons/)
		# [How to put a link on a button with bootstrap?](https://stackoverflow.com/questions/36003670/how-to-put-a-link-on-a-button-with-bootstrap)
		header = tags$div(class = "group-output",
		lapply(1:len(index), function(i) {
			idate = format(zoo::index(data[index[i]]), '%b %d, %Y')
			tags$a(href=paste0('#mdetailB',i),class="btn btn-outline-primary", idate)			
		})
		)
		
		plot_output_list <- lapply(1:len(index), function(i) {
			idate = format(zoo::index(data[index[i]]), '%b %d, %Y')
			#cat('detailsMonthCharts', i, '\n')
			plotname <- paste0("plot", i)
			tags$div(class = "group-output"			
				,tags$br()
				,tags$span(class='anchor', id=paste0('mdetailB',i))
				,tags$h3(paste(idate, 'Historical Performance:'), tags$a(href="#top", icon('arrow-circle-up')))			
				,plotOutput(plotname, height = "400px")
			)
		})

		# Convert the list to a tagList - this is necessary for the list of items
		# to display properly.
		do.call(tagList, c(list(header), plot_output_list))
	})	

	
	#[dynamically add plots to web page using shiny](https://stackoverflow.com/questions/15875786/dynamically-add-plots-to-web-page-using-shiny)
	#[Dynamically display images from upload in Shiny UI](https://stackoverflow.com/questions/33526256/dynamically-display-images-from-upload-in-shiny-ui)
	# Call renderPlot for each one. Plots are only actually generated when they
	# are visible on the web page.
	observe({
		# !!! in observe event we have to capture the error unlike in render functions
		# control if any action needs to be done
		if(is.null( ( data = tryCatch(getData1(), error = function(e) NULL)  ) )) return(NULL)				
		if(is.null( ( rets = tryCatch(getReturns(), error = function(e) NULL)  ) )) return(NULL)				

		
		
  		
		for(i in 1:len(rets$index1)){
			#cat('observe', i, '\n')
			# Need local so that each item gets its own number. Without it, the value
			# of i in the renderPlot() will be the same across all instances, because
			# of when the expression is evaluated.
			local({
				my_i <- i
				plotname <- paste0("plot", my_i)
				
				output[[plotname]] <- renderPlot({
					# control if any action needs to be done
					if(is.null( (data = getData1()) )) return(NULL)				

					calpat.technical.plot(getStocks(), getReturns(), my_i, 
						input$nLookBack, input$nLookForward)
				})
			})
		}
	})

	
	
    #*****************************************************************
    # Help Tab
    #******************************************************************    
	output$helpDetails <- renderUI({			
		src = file.path(system.file('www', package = 'rtsviz.calpat'), 'html', 'calpat.html')
		HTML(join(readLines(src),'\n'))
	})
	
	
	
	
    #*****************************************************************
    # Download
    #******************************************************************    
    # Download pdf report
	output$downloadReport <- downloadHandler(
    	filename = 'report.pdf',
    	content = function(file) {
			# control if any action needs to be done
			if(is.null( (data = getData1()) )) return(write.file('No data loaded.', file=file))
			
    		grDevices::pdf(file = file, width=8.5, height=11)
      			
			# setup
			ret.mat = getReturnsMatrix()
			symbol = getStocks()
			
			# summary plot
			calpat.overlayPlot(symbol, ret.mat)
			
			# summary trade table
			add.pdf.table(getTradesTable())
			
			# longest trade plot
			calpat.tradesPlot(symbol, ret.mat)

			# plot stats for each period
			rtsviz::seasonality.makeStatsPlot(symbol, getStatsTable()$raw
				,spl('Sharpe,% Positive,Risk,Avg')
				,paste0('(', input$frequency, ')')
			)

			# table stats for each period
			add.pdf.table(getStatsTable()$format)
			
			# historical trades table
			add.pdf.table(getHistoricalTable())

      		
		    grDevices::dev.off()
    	}
	)	
		
	# Download csv data
	output$downloadData <- downloadHandler(
    	filename = 'data.csv',
    	content = function(file) {
			# control if any action needs to be done
			if(is.null( (data = getData1()) )) return(write.file('No data loaded.', file=file))
			
			# summary trade table
			add.csv.table('Detail Historical Performance:', getTradesTable(), file, FALSE)
			
			# table stats for each period
			add.csv.table('Summary Table:', getStatsTable()$format, file)			
			
			# historical trades table
			add.csv.table('Historical Table:', getHistoricalTable(), file)			
    	}
  	)	


	# Download Rmd template
	output$downloadRmd <- downloadHandler(
    	filename = 'calpat.Rmd',
    	content = function(file) {
			# control if any action needs to be done
			if(is.null( (data = getData1()) )) return(write.file('No data loaded.', file=file))

			
			s = read.file(system.file('template', 'calpat.Rmd', package='rtsviz.calpat'))
			s = gsub('<source>',data_mod$getDataSource(), 
				gsub('<symbol>',getStocks(), 
				gsub('<userfile>',data_mod$getUserFile(), 
				
				gsub('<dates>',data_mod$getDates(), 
				gsub('<date.source>',input$dateSource, 
				
				gsub('<date.expr>',input$dateExpr, 
				gsub('<fred.id>',input$fredID, 
				gsub('<date.userfile>',input$userDatefile, 
				
				gsub('<look.back>',input$nLookBack,
				gsub('<look.forward>',input$nLookForward,
				
				gsub('<frequency>',input$frequency,
				gsub('<allow.overlap>',input$allowOverlap,
				
 				
				gsub('<filter>',input$marketFilter,
				
				s)))))))))))))
				
			write.file(s, file=file)
    	}
  	)	

	# Download csv data
	output$downloadEquity <- downloadHandler(
    	filename = 'equity.csv',
    	content = function(file) {
			# control if any action needs to be done
			if(is.null( (data = getData1()) )) return(write.file('No data loaded.', file=file))
			
			signal = getReturns()$signal
			x = quantmod::Ad(data)		
			ret = x/mlag(x)-1
			equity = cumprod(1+ifna(signal * ret,0))
			
			add.xts(equity, file)
    	}
  	)	
	
}