rtsviz.calpat
====


Sample plugin for the `rtsviz` package.
===


This user friendly shiny application allows to interactively 
explore calendar patterns. Investigate the historical impact on returns and volatility
for given look-back and look-ahead periods. User can either specify a calendar pattern or
import the historical dates for analysis.
	
	
Installation:
===

```R
remotes::install_bitbucket('rtsvizteam/rtsviz.calpat')
```
	
Example : 
===

To run this example, please install with remotes:

```R
remotes::install_bitbucket('rtsvizteam/rtsviz.calpat')
```


```R
	library(quantmod)
	
	# enable persistent time series data storage with `rtsdata` package
	# library(rtsdata)

	# load `rtsviz.calpat` package
	library(rtsviz.calpat)
	
	# start Shiny interface
	launchApp()
	
	
	# ---------------
	# start Calendar  Pattern app in the `rtsviz` interface
	# please note this package registers 'calpat' app as `rtsviz` plugin
	# ---------------	
	# load `rtsviz` package; will **overwrite** `launchApp` function
	library(rtsviz)
		
	# start Shiny interface
	launchApp(highlight.item='calpat')	
```	

